package fraktali;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JFileChooser;

@SuppressWarnings("serial")
public class Okno extends JFrame implements ActionListener {
	private Slika slikaFraktal;
	private JButton gumb, nazaj, shrani;
	private JTextField potenca;
	private int zaslonX, zaslonY;
	private int d; // predstavlja potenco
	private JFileChooser fc;
	
	public Okno() {
		// slika se ustvari �ele ko uporabnik nastavi potrebne spremenljivke
		super("Mandelbrot Explorer");
		this.d = 0;
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Dimension zaslon = toolkit.getScreenSize();
		zaslonX = zaslon.width;
		zaslonY = zaslon.height;
		slikaFraktal = new Slika(zaslonX, zaslonY);
		setLayout(new GridBagLayout());
		GridBagConstraints s = new GridBagConstraints();
		this.fc = new JFileChooser();
		// dodaj sliko
		s.fill = GridBagConstraints.BOTH;
		s.gridy = 0;
		s.gridx = 0;
		s.weightx = 1.0;
		s.weighty = 1.0;
		s.gridwidth = GridBagConstraints.REMAINDER;
		s.anchor = GridBagConstraints.CENTER;
		add(slikaFraktal, s);
		// dodaj napis 'Vpi�i potenco'
		s.gridy = 1;
		s.gridx = 0;
		s.weightx = 0.0;
		s.weighty = 0.0;
		s.gridwidth = GridBagConstraints.NONE;
		add(new JLabel("Vpi�i potenco:"));
		// dodaj textfield
		s.gridy = 1;
		s.gridx = 1;
		s.weightx = 1.0;
		s.weighty = 0.0;
		s.gridwidth = GridBagConstraints.HORIZONTAL;
		potenca = new JTextField();
		add(potenca, s);
		// dodaj gumb
		s.gridy = 1;
		s.gridx = 3;
		s.weightx = 0.0;
		s.weighty = 0.0;
		s.gridwidth = GridBagConstraints.NONE;
		gumb = new JButton("Nari�i");
		gumb.addActionListener(this);
		add(gumb);
		// dodaj gumb za zoom out
		s.gridy = 1;
		s.gridx = 2;
		s.weightx = 0.0;
		s.weighty = 0.0;
		s.gridwidth = GridBagConstraints.NONE;
		nazaj = new JButton("Nazaj");
		nazaj.addActionListener(this);
		add(nazaj);
		//dodaj gumb za shranjevanje
		s.gridx = 4;
		s.gridy = 1;
		s.weightx = 0.0;
		s.weighty = 0.0;
		s.gridwidth = GridBagConstraints.NONE;
		shrani = new JButton("Shrani");
		shrani.addActionListener(this);
		add(shrani);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == gumb) {
			d = Integer.parseInt(potenca.getText());
			slikaFraktal.nastaviInt(d, 40);
			slikaFraktal.nastaviDouble(-3.0, 3.0, 0.0);
			slikaFraktal.risanje();
			return;	
		}
		
		if (e.getSource() == nazaj) {
			double x1 = slikaFraktal.x1;
			double x2 = slikaFraktal.x2;
			double x_sred = (x1 + x2) / 2;
			double dx = (x2 - x1) * 2;
			
			double y1 = slikaFraktal.y1;
			double y2 = slikaFraktal.y2;
			double y_sred = (y2 + y1) / 2;
			slikaFraktal.nastaviDouble(x_sred - dx, x_sred + dx, y_sred);
			slikaFraktal.risanje();
			return;
		}
		
		if (e.getSource() == shrani) {
			int returnVal = fc.showSaveDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File dat = fc.getSelectedFile();
				slikaFraktal.shraniSliko(new File(dat.getPath() + ".jpg"));
				System.out.println("datoteka uspe�no shranjena");
			}
			return;
		}
	}
}