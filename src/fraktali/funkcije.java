package fraktali;

import java.awt.Color;

public class funkcije {
	
	
	public static double[] nastaviVelikost(double x1, double x2, double y_sred, int velikostX, int velikostY) {
		double yNov = (x2 - x1) * velikostY / (2 * velikostX);
		double[] popravljenY = {y_sred - yNov, y_sred + yNov};
		return popravljenY;
		
	}
	
	public static void dodajRdeco(float[] rgb) {
		rgb[0] += 0.1;
	}
	
	public static void odstraniRdeco(float[] rgb) {
		rgb[0] -= 0.1;
	}

	public static void dodajZeleno(float[] rgb) {
		rgb[1] += 0.1;
	}
	
	public static void odstraniZeleno(float[] rgb) {
		rgb[1] -= 0.1;
	}
	
	public static void dodajModro(float[] rgb) {
		rgb[2] += 0.1;
	}
	
	public static void odstraniModro(float[] rgb) {
		rgb[2] -= 0.1;
	}
	
	public static void izracunajBarvo(float[] rgb, int k, int n) {
		double a = (double)k/(double)n * 40;
		int i = 0;
		while (i < a && i < 10) {
				dodajZeleno(rgb);
				i += 1;
		}
		while (i < a && 10 <= i && i < 20) {
				odstraniModro(rgb);
				i += 1;
		}
		while (i < a && 20 <= i && i < 30) {
				dodajRdeco(rgb);
				i += 1;
		}
		while (i < a && 30 <= i && i < 40) {
				odstraniZeleno(rgb);
				i += 1;
		}
		for (i = 0; i < 3; i++) {
			if (rgb[i] > 1) {
				rgb[i] = 1;
			}
			else {
				if (rgb[i] < 0) {
					rgb[i] = 0;
				}
			}
		}
	}
	
	public static int HSBBarve(int k) {
		if (k < 50) {
			float bright = k / 50;
			return Color.HSBtoRGB(0, 1, bright);
		}
		else if (k < 490){
			float hue = (k - 50)/2;
			return Color.HSBtoRGB(hue, 1, 1);
		}
		else {
			float bright = (k - 490) / 210;
			if (bright > 1) { bright = 1; }
			return Color.HSBtoRGB(270, 1, bright);
		}
	}

}
