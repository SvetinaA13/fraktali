package fraktali;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.*;
import java.io.File;
import java.awt.Color;

import java.awt.Dimension;
import java.awt.Graphics;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Slika extends JPanel implements MouseListener {
	/**
	 * ustvari seznam pik velikosti 1920 * 1080
	 * d >= 1 je potenca funkcije f(z) = z^d + c
	 * n je stevilo iteracij
	 * [x1, x2] * [y1, y2] je obmocje v kompleksni ravnini
	 * velja x1 < x2 in y1 < y2
	 */
	
	private int d, n; //d je potenca, n je število iteracij
	public double x1, x2, y1, y2;
	private BufferedImage slika;

	public Slika(int velikostX, int velikostY) {
		super();
		addMouseListener(this);
		slika = new BufferedImage(velikostX, velikostY, BufferedImage.TYPE_INT_RGB);
	}
	
	public void nastaviInt(int d, int n) {
		this.d = d;
		this.n = n;
	}
		
	public void nastaviDouble(double x1, double x2, double y_sred) {
		this.x1 = x1;
		this.x2 = x2;
		double[] y = funkcije.nastaviVelikost(x1, x2, y_sred, slika.getWidth(), slika.getHeight());
		this.y1 = y[0];
		this.y2 = y[1];
	}
	
	public void shraniSliko(File dat) {
		try {
			ImageIO.write(slika, "jpg", dat);			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void narisiSliko() {
		double narastekX = (x2 - x1)/slika.getWidth();
		double narastekY = (y2 - y1)/slika.getHeight();
		for (int i = 0; i < slika.getWidth(); i++) {
			for (int j = 0; j < slika.getHeight(); j++) {
				double w = x1 + i*narastekX; // seznam predstavlja kompleksno število
				double z = y1 + j*narastekY;
				double x = 0;
				double y = 0;
				int k = 0; // steje kdaj absolutna vrednost preseze 2
				while (x*x + y*y < 4 && k < n) {
					double xtemp = x;
					double ytemp = y;
					int potenca = 1;
					while (potenca < d) {
						double xtemptemp = xtemp*x - ytemp*y;
						ytemp = xtemp*y + x*ytemp;
						xtemp = xtemptemp;
						potenca += 1;
					}
					x = xtemp + w;
					y = ytemp + z;
					k += 1;
				}
				if (k == n) {
					slika.setRGB(i, j, new Color(1, 1, 1).getRGB());
				}
				else {
					float[] rgb = {0, 0, 1};
					// izračunaj barvo pike glede na hitrost divergence
					funkcije.izracunajBarvo(rgb, k, n);
					
					slika.setRGB(i, j, new Color(rgb[0], rgb[1], rgb[2]).getRGB());
				}
				repaint();			
			}
		}
	}
	
	public void risanje() {
		Runnable narisi = new Runnable() {
			public void run() {
				narisiSliko();
				repaint();
			}
		};
		System.out.println("risanje");
		new Thread(narisi).start();
	}
	
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(slika.getWidth(), slika.getHeight());
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int w = slika.getWidth();
		int h = slika.getHeight();
		int x = (this.getWidth() - w)/2;
		int y = (this.getHeight() - h)/2;
		g.drawImage(slika, x, y, w, h, Color.BLACK, null);
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
			double dx = x2 - x1;
			double dy = y2 - y1;
			double x = e.getX()*dx/slika.getWidth() + x1;
			double y = y1 + e.getY()*dy/slika.getHeight();
			double x2 = x + dx/8;
			double x1 = x - dx/8;
			nastaviInt(d, (int)(n*1.1));
			nastaviDouble(x1, x2, y);
			risanje();
			return;
	}
}
