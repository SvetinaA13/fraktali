Program za raziskovanje po Mandelbrotovi mnozici za poljuben naraven eksponent.

Datoteke in Mape:
  - src\fraktali .............. vsebuje *.java datoteke:
    - Slika.java .......... risanje slike
    - funkcije.java ..... pomozne funkcije
    - Okno.java ........ glavno okno
    - Program.java .... program, ki zazene okno
  - bin\fraktali ...............vsebuje *.class datoteke z istimi imeni:
    - Slika.class
    - funkcije.class
    - Okno.class
    - Program.class